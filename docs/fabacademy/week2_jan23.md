#PROJECT MANAGEMENT

Assignment

- Build a personal site describing you and your final project.
- Upload it to the class archive.
- Work through a git tutorial.

·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.

· Building a personal site

As I have been involved in the Master's programm we already were asked to develop and create our personal website before. When I started I had no idea of how to make it, I have never done a website from scratch so I decided to work with the site powered by mkdocs and material for Mkdocs. Future: create a minimalist HTML page from scracth with the basic info I want to see in it such as the same that I have right now, I will use some cool tools such as https://www.w3schools.com/ where they have tons of tutorials and information for HTML. (· Work through a git tutorial.)


I am Writing the code with ATOM and this is how it looks like so far:

extra.css: this helps to have the same style in all the .yml files, where I have defined everything before such as how the headers are going to be, the paragraphs, the colour and background characteristics, the way that the page is disposed and divided, etc:
![](images/CSS1.JPG)
![](images/CS2.JPG)


mkdocs.yml: here what I am doing is organizing the info and adding the links that I want to see in the website.
![](images/MKDOCS.YML1.JPG)
![](images/MKDOCS.YML2.JPG)

My website is a blog where I can write down my docummentation about the Master and also the respective docummentatino of every week assignment from FabAcademy. This is how it looks like:

The corresponding aboutme page with a sort description of me:
![](images/FA1.JPG)

The corresponding link to the FabAcademy section where I will be writing the week asssignments:
![](images/FA.JPG)


**BASICS for WRITING CODE w/ atom**
h1 { HEADER
  p1 { PARAGRAPH
    ** ** bold text
    * * italic text
    ![]() to add an image/gif
             space before paragraph creates a window w/ the text, it looks like this:
             ![](images/E.JPG)



**Basic Manipulation**
There are a handful of cool keybindings for basic text manipulation that might come in handy. These range from moving around lines of text and duplicating lines to changing the case.

Ctrl+J - Join the next line to the end of the current line
Ctrl+Up/Down - Move the current line up or down
Ctrl+Shift+D - Duplicate the current line
Ctrl+K Ctrl+U - Upper case the current word
Ctrl+K Ctrl+L - Lower case the current word

**Brackets**
I use them a lot. Atom ships with intelligent and easy to use bracket handling.Atom will also automatically autocomplete [], (), and {}, "", '', “”, ‘’, «», ‹›, and backticks when you type the leading one.


For the content that I have such as the images, I have created in every folder a corresponding images sub-folder in which I have upload the images I will be using for every week:
This is how the project looks like in atom, and how the link and images from FabAcademy are organized:
![](images/FAD.JPG)


· Upload it to the class archive

I am using Git for Windows and I installed Git Bash.

Git is a distributed VERSION CONTROL system. It is usefull for collaboration, backups, reverting and merging changes made by different people.
We use GIT, a software built by Linus Torvalds to manage the Linux kernel source files.
GIT can be used to track any kind file. Git can work locally and can talk to a remote server for exchanging files and versions.

![](images/GR.JPG)

I upload and update my website in the Git repository. I was introduced to Git at the begginning of the course, so we had already a GitLab account created. I am the developer with my corresponding repository.

kEY CONCEPTS:

- Repository
- Commit
- Branches
- Pulling and pushing, merging, rebasing
- Pull requests
- Tags

We will use GitLab for all the content related to Fab Academy.

Account setup:
- Create an account in Gitlab
- Log in
- SSH Keys: Git uses the SSH protocol to establish secure communication between your computer and the GIT server hosting your repository. This makes sure only you can update your repository and nobody else can tamper the communication.SSH Keys allow you to use the GIT repository securely, without requiring a password every time.
- Add the public Key to the profile
- Clone the archive: this allows to store the repository in our desktop folder.





Uploading files_
They way I work usually when I have to upload my files: ATOM. I use it for everythimg. There are different ways for uploading files, such as using Git Bash or editing directly in Gitlab.

Atom:

![](images/GRA.JPG)
![](images/GRA2.JPG)
![](images/GR3.JPG)


Git Bash:

I don't use it but these are the git commands that I will need if at some point I want to use it:
ls: to list the contents in the file you are in
cd: to move from file to file
du -sk * | sort -n: to check the file-size of your folder
git pull: to download the newest version of the files
git status: to show status of files
git add .: to add all the files to the gitlab repository
git commit -m "text": to create a new commit
git push: to upload all the changes the gitlab repository

![](images/GB.JPG)

GitLab:
1. Log in in your GitLab account
2. Open your PROJECT
3. Go to Respotiroy
4. Click files
5. Select the file you want to edit

![](images/GRE.JPG)
