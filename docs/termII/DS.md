
![](images/P1.JPG)

![](images/P2.JPG)

....

![](images/DS_P1.png)

![](images/DS_P3.png)


![](images/P14.JPG)


#MATERIAL USED FOR THE PRESENTATION#

![](images/P12.JPG)

The mini-interventions proposed where a way to explain the S1. With the questions mentioned bellow, I was trying to get out a personal experience of involving myself in the designing and production part while working with technology and new materials. I was trying out if these proposals could lead to the transition from S1 to S2. Thinking in concepts such as organic transformations and reusing approaches.

![](images/DS_Q.png)

![](images/DS_DA.png)


![](images/DS_3D.png)
![](images/DS_I1.png)

![](images/DS_O.png)
![](images/DS_I2.png)


This mini-intervention could not be shown as it broken itself before the presentation.
![](images/DS_I3.png)


The 4th intervention where conversations with people, experts and potential users. The reflection of it is included at the end of every intervention.


#REFLECTIONS AND POINTS OF IMPROVEMENT#

**Go natural and simple**
I will go definiteviley much more natural and simple when presenting for the final presentation. I was too focused in the complex language behind the whole story and it needs to be much more lighter, which will lead to something much more brighter. I will try to avoid complex ways of showing the information and communication. People need to understand what I am communicating without doubting, it is my bridge to other people.

**Articulate the presetantion more visual: storytelling through a vast amount of images in order to provoke**
This leads to the point from above. I was missing much more visual language which will have helped visitors to understand and connect better with my idea. Visual data and images are essential.
We process images much more quickly than we processes a similar amount of written information...

**What does it mean to be sustainable? Sustainability approach, failure and success**
When sustainability can hold failure and success at the same time. It can happen that even having the best intentions it could fail in a way that we had not contemplated before. Reconsider the concept of sustainability and what does it mean to work sustainable.

**Think about present and future collaborations**
Collaborations will help the communication of my project and also the alignment of communities. I am not an expert in the area that I am interviening, then I really need to create connections that lead me to develop my project. It's quite difficult to search and contact collaborators as usually the project is in constant movement and change. I will try, though, to contact the people I think may be helpful for my project.

**Limit the framework: brand and design** + **Pack the whole presentation and go deeper**
This leads to the point from above. It is crucial to identify in the things I can not focus at this moment and limit the framework to something that can be made for the final presentation. I will go into something that is packed and very specified and realistic for the final presentation.

**Seduce with result matrices**
Matrices are always a nice way of presenting the data. I will get this advice of presenting all the researched data that I get during the project's develompment.

**Potential customers: get to know them properly, create a user's profile** + **Get potential customers involved/create a story with them**

I will focus the next weeks in working with them in parallel. Mark or identify the potential user means being aware of consequences.

**What’s my riskiest and worst assumption?**
One of the best advices I got at the presentation. This will help me to structure better the next 6 weeks period. My mission is clear: it's question is defined, its responses are not defined yet. For that, I will explore different ways of answering it, by taking the advice of going through the riskiest and worst assumption, so then I could jump into the next one knowing that the others didn't work out or as expected. Such as cleaning the land and creating space for the next thing.


**JUMP FROM THE S1 TO S2**
Personally this is the most useful feedback I got. It really had an impact on me and clear my mind a lot in terms of packing and being specific about what I really want to get out of it.

![](images/P12.JPG)

What I showed in the presentation was trying to understand and frame the S1, where the transition from unsustainable to sustainable identities have space to happen, to occur. Through those 4 microinterventions somehow I proved that this transition is possible and it is available.

S2 is another story. It is giving VALUE SYSTEM: translate human space (Algorithm). It is talking about experience.

An example of this could be the fact of people spending time with themselves such as meditation. In this step of development I do really need to be aware of how the human being feels (foots in case of the presentation), how does it mean to feel human, which are the human needs.


*How human being feel X?
How does it mean to feel human?
Which are the human needs of excepting, experience and in the Anthropocene?*

Nice example I wrote in my notes: "How does it feel to walk as ancestros?"



#Actions that I will take in a 6 weeks period (INTERVENTION PROPOSAL FOR THE NEXT TRIMESTER)#

Intervention Development PROPOSAL
*achievement of the transition from the sustainable individual identity to the sustainable collective identities*

"Humans make sense of behavior when they colllect sets of reasons and assemble them into an IDENTITY. Our sense of another's identity (Set of shared values) is what justifies cooperation."

*How does the human feels?
What does it mean to be human?
Which are the human needs?*


[1] **Giving value system - Collecting information**
· Where does the value come from?
Personal experience | Personal reflection

· Why do we have values?
Values | Heuristics

· When are values reconfigured, reconciled, refiled, reprioritized and updated?
New information | New stimulus

· How to communicate "a new information" or generate "a new stimulus" to the audience?
Emotions

· When is a new value generated?
Experience

· How to identify my own values?
Understanding myself | Understanding the others

· When is fair cooperation built?
Values | Outcomes (game theory?)

· Which are the social life values?
Values that make sense

· What does people find meaningful?
Wisdom | Life of meaning

· Which are mechanism that are able to coordinate true cooperation?

**Which classification is better to desgining social systems that create space for human values towards chlothing?**

[2] **Natural and simple communication of the project**
· Website Development
· Emails Template Creation
· Discourse

[3] **Sexy data**
· Organize past data
· Apply methods, techniques and tools adquired during the seminar Atlas of Weak Signals
· Structure and organize how the data will be displayed

[4] **Reconsider sustainability**
· What does it mean to be, feel, work, design and think in terms of sustainability?
· Which are the opinions of experts who have failed or sucess?
· How can be built the user's profile of sustainability? How to design for different considerations of sustainability? How to focus in one profile?

[5] **Collaborative connections - Vital for the evolution of the project**
· Current situation
· Future of the project

[6] **Potential customers**
· Involve myself and the project with the potential customers
· Articulate its steps with people experiencing them

[7] **Fab Academy**
· Create weekly simple artifacts that lead to personal experimentation or personal reflection
· Prepare the final presentation at the very beginning with them
· Discuss about what is possible in the near future

[8] **Translate human space (Algorithm)**
· Source Data
Match data with the issue | Capture the diversity of the space | Keep humans in the loop | Clean up messy data
· Dataset Creation
Training | Validation | Testing sets | Manual splitting
· Prepare the data
· Evaluate
· Test the model



Key moves
- Go with the craziest ideas for first: try out the riskiest and worst assumption ASAP
- Contact with the faculty on tuesday to structure it better
- Create full time practical research
- Efficient moves: time flies!


![](images/DS_S.png)
