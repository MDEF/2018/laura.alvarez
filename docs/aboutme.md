#"Hello, it's ˈlɔːrə . What's going on out there?"

![](termI/images/ho_hey.jpg)

(r)evolution is the key_ I am who I evolve so I can't say I am this because tomorrow I will be something else. Apart from that, I could say I have a background in Marketing and Market Research but most of the time I have been discovering and exploring all around the globe...
I am fascinated about nature; its power captures me. Textures. Simplicity. When I observe and contemplate the world through a camera I can describe easily what stops me to capture that beauty: perspective, composition, lightness, movement...

I love the color white and palms. Buildings and the sky. The sea makes me feel at home and the blue sky makes my day every second I look up there.

![](termI/images/sea.JPG)
![](termI/images/palm.JPG)
