
Abstract
> In order to create a crazy and full of randomness atmosphere for our class-studio, we have been going through an introduction to physical computing by hacking everyday objects and learning the core principles which will help us in the future to be creative. Thus, we have gained knowledge to change the way things used to work and transform them into our way, by changing also our perspective about how things interface with the world.

·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.

the way things WORK [] GO [] SEE [] FEEL [] REACT


![](images/twtw.JPG)

· Pre-Design

For this project of experimentation and playing with the computing skills that we were getting during the week, we did a brainstorming as an initial phase of the process. It came up the idea of identity how happy were the people when they attend class or either spend time in the studio: the physical response to laugh. How much laugh is produced in the class every day? Can we identify the mood of the people measuring it? Which is the physical response to laugh, is it joy?  How are we going to measure it? Could it be by creating a pattern of the laugh from the frequencies? Which are the waves of happiness? We tried to develop this first idea that we had with an analog microphone but we faced many challenges to keep going with it as these sort of microphones won't give us the patterns that we were looking for as they detect small sound.

Although we couldn't develop this project further because of the lack of time, it is quite interesting for me the knowledge we acquired about the sound, its patterns, and measurements.

Here there is a sketch of Low‑pass filters and high‑pass filters. It helps to remove unwanted rumbles and other unwanted sub‑sonic rubbish that microphones tend to capture to focus on our desire 'waves'/patterns that are created with the sound of the laugh.

![](images/sonido.jpg)

Moving from an INPUT to an OUTPUT we found interesting that we could focus in  LIGHT, COLOR & MOVEMENT. We were thinking about creating a light sculpture installation around the studio and making it interactive when people were around. Some of the references are the following ones:

![](images/inspiración.JPG)

. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

· Concept Design

Our concept was a light sculpture installation with movement. We started to think about how we will be able to get the light that we were looking for and the movement as well. The questions where though: How can we connect movement with light? How can we translate a movement into the light? Is it possible to move objects up and down or should we move them in a linear way? Are we trying to translate the movement into light signals? What do we have available in the class?

![](images/borrador.jpg)

![](images/borradormotores.jpg)

![](images/R.jpeg)


. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

· Design development

**Output:** LIGHT [] COLOR [] MOVEMENT

_MOVEMENT_

**Hardware required:** Arduino Board, 4x Servo Motor, Hook-up wires

**Materials:** branches from the floor, tape & some plastic

**Circuit:**

![](images/circuitservo.JPG)

**Code:**

To consider: before void setup you can create a variable named angle max, which will help you when typing the rest of the code. Once you write this down, you can always say angle max and keep it simple. Ours was 180 as we were looking for the movement of a wave.

Start the four servos to move (begin).
![](images/voidsetup.JPG)

Type how will they move. This is a nice way to move something as you can use some of them and create the movement that you are exactly looking for. It is quite easy to try different forms.
![](images/voidloop.JPG)

**Prototyping:**

![](images/prototyping.JPG)
![](images/prototyping1.jpeg)



_LIGHT & COLOR_

**Hardware required:** Arduino Board, Addressable RGB LEDs, Hook-up wires

**Materials:** thread, mirror

**Circuit:**

![](images/circuitleds.JPG)

**Code:**

Download library: https://drive.google.com/open?id=1rCupwU1zuiRC_EsW_RMrd6V-rO1zuUyL
Type of LED strip used for the exercise ws2812b

![](images/codeled1.JPG)
![](images/codeled2.JPG)
![](images/codeled3.JPG)
![](images/codeled4.JPG)
![](images/codeled5.JPG)
![](images/code6.JPG)

**Prototyping:**

![](images/prototypingled.JPG)
![](images/GIF1.gif)

. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

· Final Design

Waves movement in blue light that reflects in the mirror for being able to appreciate it amplified.  

![](images/installation.JPG)
![](images/w7FD.jpeg)


Because of the lack of time, we couldn't develop the initial concept. We had to choose one color for our waves and also it was static. We were not able to make it interactive when people were moving around. Another thing we didn't get was the connection between light and movement at the same time.

Although things worked in a different way as expected, it was such a great week where we were in contact with how things work! Now we have gained this knowledge it is easier to start to grow some experiments on our own and applying these concepts to our final project...


...


*Learning the core principles is awesome as then it can help you to be more creative in our project development.*

*I understood that everything can be made by hacking the things we have within our atmosphere.*

*I feel comfortable enough to develop a project similar like this and experiment by myself.*

*I could appreciate how crucial is to know how things are meant to be and how we can transform them to create our own reality in terms of everyday objects.*

*It helped me to amplify my perspective and start seeing the complex things as simple things. Everything, although it seems like a complex machine, it is a simple object that can be made whenever. This can be connected with the feeling of looking for standardization in every step.*

. . . . . . . . . . . . . . . . . . . . . * . * . * . . . . . . . . . . . . . . . . . . . . . . . . .

          References
            [1] https://www.arduino.cc/en/Tutorial/HomePage
            [2] https://hackmd.io/ys7lcCDJSeq69meSeGnIXg?view#References
            [3] https://www.grimanesaamoros.com/
            [4] https://drive.google.com/open?id=1rCupwU1zuiRC_EsW_RMrd6V-rO1zuUyL
            [4] https://www.pinterest.es/prangg/light-installation-art/
