
Abstract
> Designing with extended intelligence imply responsibilities, challenges and a whole world of uncertainty. Through an intense week full of information, debate, questions without answer and practice we are closer to the world of AI/ML and now we have a better notion of what it means being part of this new design era.

·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.

**Deconstructing intelligence**

        When thinking about what intelligence is, a lot of ideas came up -having the capacity to process and evaluate information, to make decisions, to experiment diverse feelings, to dream, to communicate with others and with the environment, to learn and adapt, also to understand, self-awareness, logical thinking, being able to connect and share information. Every human being has its own intelligence, which is developed through evolution in a different form. So every human has a different kind of intelligence, we all share, though, biological intelligence. It becomes different in the way we evolve or develop ourselves. Having a purpose or a goal also means having intelligence. Our brain operates in a collective dimension, somehow.

Perhaps we can't define exactly what intelligence it, but we can ask some questions and try to understand how it works. Questions such as Are we going to design with intelligence? Or are we going to design for intelligence? How much of this intelligence are we going to project into things
? How do we measure human intelligence? Are we behaving intelligently? Do we act with prediction or intuition?

Some of the factors of human being that make him acting with intelligence are such as Perception [] consideration []  intuition [] empathy (ethical and moral issues) [] Making decisions [] Link information [] Processing information [] Attachment [] Empathy & intuitive thinking

**Dispelling the AI myth**

Rationalization and logic coexist with deep myth. There is an ancestral belief in invisible forces that we are told that are better thinkers than us. The user is just a content creator, a content provider that is being told what to do and with the path to follow, without even understanding what is behind of every outcome that is receiving. They tend to think that algorithms are efficient, logical and clean procedures where they are not.

What algorithms can do is to create a parallel reality for the user that operates as a force of control, is not transparent at all.

AI is such as the new controlling force for the society although it is seen as a piece of technical magic.

**Irrationality of the users**

The human aspects of the interactions between human and machines must be considered when designing with AI. The consumer is irrational by nature and as designers or creators, we need to consider these human aspects as we will impact in them. We should always look for the positive adaptations and the higher wellbeing level with our interventions when designing AI systems. The relationship between the algorithm accuracy and the user response to the algorithm's presentation and context has to be taken into account.

Users may see the algorithm as omniscient. As it is seen the algorithm is able to see through their represented selves into their internal selves, which is completely an irrational thought. The irrationality of these agents make them feel such as intelligent machines have to be accurate but they don't know how is it be operating, if in a non-accurate way or the opposite. This should be showed by who designs it and the designer needs to pay attention to the context that is applying it. Users do absurd rating because of its irrational behavior. They tend to confirm what they have been told rather than evaluating the system on its merits.

Two main points I will take into account when designing AI: the consequences of the algorithms we design and the way we present the results.  

**The trust of users to AI/ML**

Users see it as a piece of magic. They give credit to the reality that the systems are creating for them. They choose which system they want to feed them. Nowadays artificial intelligence is everywhere. People give these systems the access to chose by themselves which partner is the ideal one, suggest music they may like, offers them books they may be interested in... The only job users have to do is a search for what they want, what they need and then the system will do everything for them.

What is the point of it? I mean, What is the point of this substitution? Aren't we giving to these systems too much power in our daily lives? This makes me reconsider what is the goal of AI/ML.

Users, they don't even understand how they work. They see AI/ML as a procedural problem-solving entity. But wait, what is happening? Are they really solving our problems? What does it mean having a problem? What does it mean not being able to solve something by yourself that you need external help?

**What design can do with AI/ML?**

Designing speculative scenarios where AI/ML may be included is such a new narrative for the questions that are evolving the big paradigm of this new era.  What machines can do does not coincide necessarily whit how we think. So we should imagine the evolution of AI by experimenting with it and observe what happens when the indeterminate starts playing a role. Operate at the edge of thinking.

What design may be able to do is to create new collaborative models of human-machine interactions to face the ambiguity and the uncertainty. Imagining a world where technology is co-evolving with humans.

**Does it exist artificial intelligence?**

Artificial intelligence depends on evolution. A human being has intelligence itself and it is the agent that programs the intelligence of every machine or technical objects. So if any object or machine evolves it is because a human being is behind. Humans have been co-living with technology for many years and the evolution of technology only depends on what the human do for it. This means that in the case that a possible-intelligence emerges it will depend on human goals and purposes. The intention of its inventor is what matters. Our intelligence is being evolved into external ''creatures'' that will surround us in our atmosphere. What we are creating are new authorities... that for now depend on us, while ML and AI don't evolve quicker than us by itself. Is it possible to evolve by themselves? It will depend on the intention of the human, on its intentions.

What will happen if we lived in a world where we are not prepared for this machine evolution but people make this happen? How will this effect to the whole society? How the human race will be affected? Are we thinking individually or collectively when developing and evolving the AI/ML? Who takes care about the impact of this developments?  

**Issues concerning to algorithm perception**

In the way, artificial intelligence is evolving it is impossible to know exactly what the meaning of it is and how it could be described but what we know is that the algorithms are designed by experts so they carry a responsibility in their backs. There is also a challenge that has to be faced concerning to the perception of it as people (users and future users) have a lack of information in terms of how does it work and what is the meaning behind the outcome that they get.

**Third party in algorithms making**

Who is the authority of these systems? Who has the responsibility to ''check'' the algorithms, the data that is injected into the machines? We are creating a whole system where it doesn't exist a role of authority, a third party who decides what is being created and implanted, even knowing that we are creating for people, for the citizens, and they may be affected.

Who governs the artificial intelligence, systems that are surrounding around us in our daily lives? Who takes care of the users?

**Change in consciousness perception**

The scenarios that may exist in the future with non-human entities could lead us to think about the origin of a new concept of consciousness. We can imagine instead of translating the human consciousness to machines creating a new shift: from the current notion of consciousness to a disaggregated form of it. We can explore the possibility of new forms of cognition that can exist when designing AI/ML. What can this new forms of consciousness offer? What this non-human intelligences can offer talking about good intentions? Is there a new capacity for consciousness when defining this new concept?

**Rethink and modify the actual uses and implementations of AI, embrace the potentials of ML/AI and rethink the whole design**

What can AI offer? Can AI offer new implementations such as extend our thinking into the environment through the things we interact with?

Should we think about the devices we own that they can have potential further than the usual tasks that they actually do? What if we treat smart systems as a friend? What if instead of having them to accomplish a task we think about them such as something to interact with, to communicate with, by giving them an identity? Will this help us to not to think about them as the substitution of our responsibilities or duties but as a compliment in our daily life?

For instance, autism people could have a system to communicate with and also thanks to the AI/ML it could be developed a machine able to generate the information that they inject on it and translate into any language in the world. Also, this allows them to communicate in a better interactive way and easy for them as they can't communicate by themselves with the usual word-based language. This system could be made of drawings/pictures that allow them to form sentences in a simple way for them.

What if instead of using AI/ML as a substitute for our responsibilities, duties or tasks we use it as a way of collaboration with common and competing goals?

AI/ML could build ways to increase or expand human creativity, human learning... It could be a way of expanding the human opportunities and achievements but considering it as something that can be used without replacing humans. Generate new systems that produce outcomes in form of interactions, methods, patterns.

AI/ML not to make decisions, to support humans in complex scenarios or extended scenarios where they can't develop by themselves. This is the ethical principles that designers could think about when designing these systems and ask themselves: do we want to give to machines our ethical principles?

**Preserve the human identity**

It should be an authority that supervises this individual evolution and creation of new authority entities (AI/ML) that move people. Do we as designers want to evolve this autonomous entity? Do we want to humanize these entities? Where will be our identity as humans? Are we concerned enough about the future that we are creating?
A new context requires new approaches. If we are going to focus on complex behaviors and extended interactions with multiple people and systems we should always keep in mind that we need to preserve the human identity and its participation in world decisions.

**Design challenges while designing with AI**

Designers have to face a set of challenges when they design for AI/ML. The world is technologically evolving day by day and we don't know where is the end of artificial intelligence and machine learning and when it will start the era of super-intelligence. What we know now is that AI/ML is installed in our routine.

When designing with AI/ML the most vital part of it is the data as is the food that feeds the machine, the set of rules that will make it work, the piece of information that contains how it will act in every scenario. This is the main thing designers have to play with. A big responsibility is born for every creator or designer: How we deal with data?

Why is the vital part? Because it has an impact on a societal level. It has influence in our behavior and perceptions and the irrational aspect from humans can be affected by this. AI surrounding them by causing problems, issues, etc.

As it has an impact and irrational individuals trust it as it were omniscient, we as designers should take this reality into account and have our eyes on the people. Also, not only creators of AI should be careful with the data and the outcomes but also a third authority should exist to measure the impact that these creations have in our lives and to determine which is good enough to enter in the ''market''. We need to answer a set of questions in order to avoid serious problems with our privacy/thoughts...:
Who is there to control the invisible AI systems?
Who has the authority? The creators?
What are the intentions of them?

 Designers are creators of new realities: we are entities that create another entity called smart systems.
Who is creating the AI/ML system has the responsibility of being fair. The technology is a reflection of the human that is creating it. The creative community has the responsibility to know that users are irrational people and they give trust to AI as they consider it superior. They don't question its validity in most of the cases.

''The more we delegate with AI, the further we remove ourselves from understanding what we do things''. This is the affirmation that makes me wonder about what are we designing AI for and also why it has been created for. Shouldn't AI give us significant and meaningful experiences? Should we focus on how people experience and how this has an impact on our design process?

AMPLIFY rather than SUBSTITUTE. AI should be designed with the purpose of amplifying human capabilities instead of substituting its capabilities and make the human completely useless. Helping designers in the creative process to allow them to become more efficient in their works by helping them in diverse complex processes.

![](images/w6A.JPG)

AI/ML needs to be consistent, efficient and designed for extending capabilities.


**Future work**

Care must be taken in designing applications that can explain algorithms.

The future is uncertain so when thinking about designing with AI means speculating and imagine a world where this could happen. Interventions with AI as co-evolution of humans and technology. Constructing futures together and having a tangible impact. Collaboration, Co-evolution and thinking about artificial intelligence as something indeterminate and imaginative.

Focus on speculative approaches to create a material to work with. Shaping the future in terms of speculating in a professional way.

![](images/w6B.JPG)

Focusing on the performative strategies more than in the descriptive ones -prompt, problematize, inject ambiguity and non-rational and non-sensorial. Affirm the possible. Configure new figures of thought by speculating. Answering the question: what is ML doing today and what will be able to do tomorrow?

![](images/w6C.JPG)
![](images/w6D.JPG)
![](images/w6E.JPG)
![](images/w6F.JPG)
![](images/w6G.JPG)


. . . . . . . . . . . . . . . . . . . . . * . * . * . . . . . . . . . . . . . . . . . . . . . . . . .

          References
            [1]
            [2]
            [3]
            [4]
            [5]
            [6]
