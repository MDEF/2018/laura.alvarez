Abstract
> There is a huge challenge that has to be faced: the combination of the digital world with the physical one. The solutions for tomorrow coexist all together behind this combination and it is crucial to find a way to integrate in the systems this available technology and digital fabrication that is close to us, just waiting to be applied.


·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.

In order to create something new, a brand new system, intervention, solution or product we have the responsability to combine the knowledge that both worlds are teaching us. How we communicate all of this learnt will reflect in our success in whatever we create.

The connections that I try to create in my brain are those ones that can respond to this set of questions:

· How could we sustain all the design industry without wondering and caring about the future?

· How can we create all of this for the present without having our eyes on the planet?


Here it is my bootcamp **'zero project'**:

![](images/ZEROPROJECT.jpg)



#POBLENOU SYSTEM
What brings together all those hidden or unknown locals that we had the pleassure to visit is the social innovation that each of them are creating and implementing. A thinking based on co-working perspectives, co-developing and co-creating as the key of solution.
A huge impact to my brain was the explanation of the **indissoluble** creator about the brain. The way he described it as “this is the result of the relationship between the brain and the languages, how an architectual form can explan an inside or an emotion such all that is contemplate in a brain”. I guess that this gorgeos art of work is the result of a multidisplinary team which is holded by content creatros, graphich designers, architets and more. The connection between technology and the physical body in such an art piece that consists in mapping the responses of the brain, conneccted with a brain, with a person and whatever you feel or think it turns like that.  Also he mentioned about the key of prototypoing everything first that is crucial in every design proccess. **TransfoLab**: Now that our system is somehow broken, how can we use all those left overs that the industry and the humans are creating?



· What’s the most important part of every industry?
If I had to answer this question I would clearly say: the commitment of its individuals and developers to humankind.  The emergent future which is coming will be a little brighter if it is maintained by a network of brains such as designers with interests and ambitions both focused on that. Designers have in a way the responsibility of getting an active awareness of their commitment to humankind as a whole.
