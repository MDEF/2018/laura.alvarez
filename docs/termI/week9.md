Abstract
> The greatest ideas need to be funded what means that they require a proper communication of the project behind. Heather Corcoran and Bjarke Calvin have translated to us their knowledge by displaying useful tools and techniques to engage our own narrative.

·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.·.


**"A holistic, integrative and worldwide approach"**

What I have learnt from this intense and tough week is that behind every great idea needs to be a good communication system to reach the audience. Either for getting fund or to communicating the project it needs a proper plan behind.

. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

When I started to refine my area of interest I realized that it was quite tough for me to identify them. The journey in which I have been involved in during these past eight weeks have helped me to identify how I want to focus the actions that I will take in the future. As I have an interest in more than one area I decided to structure how will I work:

1. Each domain has an objective, a mission.
2. Quantum entanglement between each domain: the energy micro activators between each domain have the capability to initiate a revolution inside another and also to generate any kind of creative energy that enables the easy transmission to another.
3. The power of small waves, focus on them to start the revolution.
4. Two main areas of interest, the most powerful ones concerning my inner drivers: wearables and education. One will initiate the other.
5. Circular knowledge perspective.
6. The goal of circular eco-systems with networks.
7. Focus in blue ocean strategies: sustainable design strategies which can help to fill the gap in hands-on science and engineering products for poor communities; design accessible tools to improve the quality of life and empower the user.
8. The project will be developed by taking action in science, engineering, design and art.
9. The interventions will have the aim to invert the course of the resources among the time and to generate meaningful tools for those people who have more needs than resources.
10. The work plan will be split in two ways: short-term perspective and long-term perspective.
11. The project's most unique aspects: accessible, effective and sustainable

*Short term perspective*: new sustainable solutions for wearables (wearables technology and wearables computing)

*Long-term perspective*: newly sustainable landscapes for education in poor communities (give them solutions to grow by themselves, generate economy from the inside, locally manufactured, scrapyards to recover, increase the wealth instead of reducing the poverty)


. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .



![](images/W9B.JPG)

Some questions I ask to myself: how the fashion industry will evolve? How it will take place in the future? Somehow this industry is about to evolve into new solutions such as wearables and taking place with new approaches and perspectives behind, those ones that can create a connection with the human that is wearing the item; the industry will be made out of meaningful moves; it really needs a realistic point of view and the world's environment.

The digital transformation is the solution to address the problems of efficiency, validation, quality of products and the ability to focus on creativity and innovation.

We are in the new era of apparel where there is a need of educating the fashion market. Some of the interventions that can be made in this new era are those ones that help to reach better adaptation and more creativity such as artificial intelligence, virtual reality and augmented reality.


It is crucial to face the failures that this market experiments: ecological damages, waste of time, waste of fabric, manufacturing of the garment, reach global shopping audience and optimization of the **supply chain**.

     "The fashion industry is the 2nd biggest consumer of water, producing 20% of wastewater, while also generating more greenhouse gas emissions and using more energy than all international flights and maritime shipping combined"


GOAL....A new landscape for fashion: wearables design (4.0 / 5G / 5D / AI) to face the fast fashion, mass production, reduce the consumption of electricity and the waste of water, re-design habits, recycle materials and combat slavery.


**Supply chain: ink supplier**: poor availability of sustainable fabrics and components for those manufacturers that want to be greener: research supply to find a cheap solution. Sustainable products to the mass market at an affordable price to promote the eco-conscious consumer.


______________________________________________________


![](images/w9A.JPG)

I have joined recently the community of Mombasa in Kenya. During my time with them, I realized myself that I wanted to go deeper and understand the scenario better. Many things have to be solved there because the children are in danger, in lack of education, in lack of resources and in lack of wealth are.

The community I was involved with were willing to go away from their hometown as they don't perceive that they could have future there. My purpose is to develop a new atmosphere for them, to help in the journey of finding/creating a proper future in their lives inside of their community.

The focus of the interventions has to be in the increasing of their wealth instead of reducing their poverty (as it has been done before).

       "But it is not just the poor who are thus trapped. By throwing away a huge amount of potential talent and energy, the entire society condemns itself to poverty."


Collaborators are needed to take further the project. Both students and professionals can make this project happen.

GOAL....An innovative educational toolkit: "if a system is designed well, it would naturally win over the existing ones". (give them solutions to grow by themselves, generate economy from the inside, locally manufactured, scrapyards to recover, increase the wealth instead of reducing the poverty). **Materials research** to face the challenges and create solutions for existing problems.


**Materials research**: wood pyrolysis process research as a starting point.



. . . . . . . . . . . . . . . . . . . . . * . * . * . . . . . . . . . . . . . . . . . . . . . . . . .

          References
            [1]
            [2]
            [3]
            [4]
